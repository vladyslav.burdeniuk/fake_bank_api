# Simple bank prototype API

**Features:**
- account creation
- transfer amounts between any accounts
- retrieve balances for a given account
- retrieve transfer history for a given account
- details in swagger api documentation

**TODO**:
- add mass actions support
- whitenoise + gunicorn
- use postgress db on production
