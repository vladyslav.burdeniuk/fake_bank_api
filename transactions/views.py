import rest_framework_filters
from rest_framework import mixins, permissions
from rest_framework.viewsets import GenericViewSet

from transactions.models import Transaction

from .serializers import TransactionSerializer


class TransactionViewSet(
    mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet
):  # pylint: disable=too-many-ancestors
    """Transaction ViewSet"""

    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    permission_classes = (permissions.IsAdminUser,)
    filter_backends = (rest_framework_filters.backends.ComplexFilterBackend,)
    filterset_fields = ("sender", "receiver")  # usage: transactions/?filters=(sender=1)%20|%20(receiver=1)
