import uuid
from decimal import Decimal

from django.db import models


class Transaction(models.Model):
    """Transactions model"""

    uid = models.UUIDField(default=uuid.uuid1, unique=True, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=100, decimal_places=2, default=Decimal(0.00))
    # allowed blank sender for creating initial balance and depositing cash through the cashier
    sender = models.ForeignKey("accounts.Account", on_delete=models.SET_NULL, related_name="sender", null=True, blank=True)
    receiver = models.ForeignKey("accounts.Account", on_delete=models.SET_NULL, related_name="receiver", null=True)
    comment = models.CharField(max_length=256, blank=True, null=True)
    # TODO: improve transaction states field

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return str(self.uid)
