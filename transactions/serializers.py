from rest_framework import serializers

from accounts.services import NoEnoughMoneyError, money_transfer
from transactions.models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    """Transaction Serializer"""

    class Meta:
        model = Transaction
        fields = (
            "id",
            "uid",
            "created",
            "amount",
            "comment",
            "sender",
            "receiver",
        )

    def create(self, validated_data):
        try:
            transaction = money_transfer(
                sender=self.validated_data["sender"],
                receiver=self.validated_data["receiver"],
                amount=self.validated_data["amount"],
            )
        except NoEnoughMoneyError as e:
            raise serializers.ValidationError(e)
        else:
            return transaction
