# Generated by Django 3.2.4 on 2021-06-09 11:27

import uuid

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transactions", "0005_alter_transaction_uid"),
    ]

    operations = [
        migrations.AlterField(
            model_name="transaction",
            name="uid",
            field=models.UUIDField(default=uuid.UUID("aeae01f8-c915-11eb-bfba-db7323573c60"), unique=True),
        ),
    ]
