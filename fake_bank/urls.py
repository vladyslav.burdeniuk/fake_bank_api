from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework.routers import DefaultRouter

from accounts.views import AccountViewSet
from customers.views import CustomerViewSet
from swagger import views
from transactions.views import TransactionViewSet

api_router = DefaultRouter()
api_router.register(r"accounts", AccountViewSet)
api_router.register(r"customers", CustomerViewSet)
api_router.register(r"transactions", TransactionViewSet)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/", include(api_router.urls)),
    re_path(r"^swagger(?P<format>\.json|\.yaml)$", views.schema_view.without_ui(cache_timeout=0), name="schema-json"),
    re_path(r"swagger/$", views.schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    re_path(r"^redoc/$", views.schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
