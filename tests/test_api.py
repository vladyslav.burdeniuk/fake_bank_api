import json

from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework.test import APITestCase

from accounts.models import Account  # noqa
from accounts.services import create_account_with_initial_transaction
from transactions.models import Transaction  # noqa


class FakeBankApiTests(APITestCase):
    def setUp(self):
        self.superuser = get_user_model().objects.create_superuser(
            username="superuser", email="superuser@email.com", password="pass"
        )
        self.superuser.is_superuser = True
        self.superuser.save(update_fields=["is_superuser"])

        self.testuser1 = get_user_model().objects.create(
            username="ArishaBarron",
            name="Arisha Barron",
        )

        self.testuser2 = get_user_model().objects.create(
            username="BrandenGibson",
            name="Branden Gibson",
        )

        self.account1 = create_account_with_initial_transaction(name="TestAccount1", balance="10", customer=self.testuser1)
        self.account2 = create_account_with_initial_transaction(name="TestAccount2", balance="0", customer=self.testuser2)

        self.account_data = """{
             "name": "TestAccount3",
             "balance": 111,
             "customer": 1
         }"""

        transaction_data = {"amount": 1, "comment": "", "sender": self.account1.id, "receiver": self.account2.id}
        self.transaction_data = json.dumps(transaction_data)

        over_limit_transaction_data = {"amount": 100, "comment": "", "sender": self.account1.id, "receiver": self.account2.id}
        self.over_limit_transaction_data = json.dumps(over_limit_transaction_data)

        self.account_url = reverse("account-list")
        self.transaction_url = reverse("transaction-list")

    def test_account_unauthorized_forbidden(self):
        response = self.client.get(self.account_url)
        self.assertEqual(response.status_code, 403)

    def test_non_superuser_unable_create_account(self):
        self.client.force_authenticate(user=self.testuser1)
        response = self.client.post(self.account_url, self.account_data, content_type="application/json")
        self.assertEqual(response.status_code, 403)

    def test_superuser_create_account(self):
        self.client.force_authenticate(user=self.superuser)
        response = self.client.post(self.account_url, self.account_data, content_type="application/json")
        self.assertEqual(response.status_code, 201)

    def test_transaction_unauthorized_forbidden(self):
        response = self.client.get(self.transaction_url)
        self.assertEqual(response.status_code, 403)

    def test_non_superuser_unable_create_transaction(self):
        response = self.client.post(self.transaction_url, self.transaction_data, content_type="application/json")
        self.assertEqual(response.status_code, 403)

    def test_superuser_create_transaction(self):
        self.client.force_authenticate(user=self.superuser)
        response = self.client.post(self.transaction_url, self.transaction_data, content_type="application/json")
        self.assertEqual(response.status_code, 201)
        self.account1.refresh_from_db()
        self.account2.refresh_from_db()
        self.assertEqual(self.account1.balance, 9)
        self.assertEqual(self.account2.balance, 1)

    def test_transaction_not_enough_money(self):
        self.client.force_authenticate(user=self.superuser)
        response = self.client.post(self.transaction_url, self.over_limit_transaction_data, content_type="application/json")
        self.assertEqual(response.status_code, 400)
