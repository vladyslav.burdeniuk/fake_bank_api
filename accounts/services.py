from decimal import Decimal

from django.db import models, transaction

from accounts.models import Account
from transactions.models import Transaction


class NoEnoughMoneyError(BaseException):
    pass


@transaction.atomic
def money_transfer(sender, receiver, amount):

    if Decimal(sender.balance) < Decimal(amount):
        raise NoEnoughMoneyError("Not enough money on sender's balance")

    _transaction = Transaction.objects.create(sender=sender, receiver=receiver, amount=amount)

    balance_recount(sender)
    balance_recount(receiver)

    return _transaction


@transaction.atomic
def create_account_with_initial_transaction(name, balance, customer):
    account = Account.objects.create(name=name, balance=balance, customer=customer)
    Transaction.objects.create(amount=balance, receiver=account, comment="Initial balance")
    return account


def balance_recount(account):
    income = Transaction.objects.filter(receiver=account).aggregate(models.Sum("amount"))["amount__sum"] or Decimal(0.00)
    outcome = Transaction.objects.filter(sender=account).aggregate(models.Sum("amount"))["amount__sum"] or Decimal(0.00)
    account.balance = income - outcome
    account.save()
