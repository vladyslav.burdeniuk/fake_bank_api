from rest_framework import mixins, permissions
from rest_framework.viewsets import GenericViewSet

from .models import Account
from .serializers import AccountSerializer


class AccountViewSet(
    mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet
):  # pylint: disable=too-many-ancestors
    """Account ViewSet"""

    serializer_class = AccountSerializer
    queryset = Account.objects.all()
    permission_classes = (permissions.IsAdminUser,)
    filterset_fields = ("customer",)
