from django.contrib import admin

from .models import Account


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    """AccountAdmin class"""

    model = Account
    list_display = [f.name for f in Account._meta.concrete_fields]
