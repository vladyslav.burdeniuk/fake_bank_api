from rest_framework import serializers

from accounts.models import Account

from .services import create_account_with_initial_transaction


class AccountSerializer(serializers.ModelSerializer):
    """Account Serializer"""

    class Meta:
        model = Account
        fields = (
            "id",
            "name",
            "balance",
            "created",
            "customer",
        )

    def create(self, validated_data):
        return create_account_with_initial_transaction(
            name=self.validated_data["name"], balance=self.validated_data["balance"], customer=self.validated_data["customer"]
        )
