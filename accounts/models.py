from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models


class Account(models.Model):
    """Account model"""

    name = models.CharField(max_length=32, unique=True)
    balance = models.DecimalField(
        validators=[
            MinValueValidator(0),
        ],
        max_digits=100,
        decimal_places=2,
        blank=True,
        null=True,
    )
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="accounts")
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
