pwd
p=lint_env/bin/python

echo Black &&
$p -m black --exclude '/(venv|lint_env)/' --line-length=130 . &&
echo &&
echo isort &&
$p -m isort --gitignore -q . &&
echo &&
echo autoflake &&
$p -m autoflake -r -i --exclude venv,lint_env --remove-all-unused-imports --remove-duplicate-keys --remove-unused-variables . &&
echo &&
echo Flake8 &&
$p -m flake8 . &&
echo &&
echo Test &&
./venv/bin/python manage.py test
#echo Pytest &&
#./venv/bin/python -m pytest ./tests/test.py
# &&
#echo &&
#echo Pylint &&
#$p pylint --load-plugins pylint_django --django-settings-module=fake_bank.settings . &&
#echo
