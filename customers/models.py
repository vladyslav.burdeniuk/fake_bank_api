from django.contrib.auth.models import AbstractUser
from django.db import models


class Customer(AbstractUser):
    """Customers model"""

    name = models.CharField(max_length=128, unique=True)
