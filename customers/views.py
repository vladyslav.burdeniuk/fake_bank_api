from django.contrib.auth import get_user_model
from rest_framework import permissions, viewsets

from .serializers import CustomerSerializer


class CustomerViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    """Customer ViewSet"""

    serializer_class = CustomerSerializer
    queryset = get_user_model().objects.all()
    permission_classes = (permissions.IsAdminUser,)
